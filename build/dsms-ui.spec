%define _topdir @RPM_BUILD_DIR@
%define name @RPM_NAME@
%define version @RPM_VERSION@
%define release @RPM_RELEASE@
%define arch @RPM_ARCH@

Name:           %{name}
Version:        %{version}
Release:        %{release}
Summary:        DSMS UI components, providing a web-based graphical interface.
License:        MIT
BuildArch:      %{arch}

Requires: nginx

%description
UI components, providing a web-based graphical interface for user management and use of DSMS systems.
%install
mkdir -p %{buildroot}/etc/dsms/dsms-ui/
mkdir -p %{buildroot}/etc/nginx/conf.d/
cp dsms-ui.conf %{buildroot}/etc/nginx/conf.d
cp dsms.key %{buildroot}/etc/nginx/conf.d
cp dsms.pem %{buildroot}/etc/nginx/conf.d
cp -r dsms-ui/* %{buildroot}/etc/dsms/dsms-ui

%post
systemctl start nginx
systemctl enable nginx

%files
/etc/*

%changelog