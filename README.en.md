# dsms-ui

### Description
dsms-ui is a graphical user interface (UI) component for the Distributed Storage Management System (DSMS).

This component works with dsms-engine (the management engine component) to enable users to use and manage the DSMS system through web pages.
### Technology stack
- Vite
- Vue3
- Es6
- Element-plus
- Pinia
- Typescript
### Installation and use
1.  Get the project code
```
git clone https://gitee.com/anolis/dsms-ui.git
```
2.  Installation dependencies
```
cd dsms-ui
npm install
```
3. run
```
npm run dev
```
4.compile
```
npm run build
```
### Browser support
This project theory supports the use of the following versions and later versions of the browser
| Edge      | Firefox      | Chrome      | Safari      |
| --------- | ------------ | ----------- | ----------- |
| Edge ≥ 79 | Firefox ≥ 78 | Chrome ≥ 64 | Safari ≥ 12 |
> Since Vue 3 no longer supports IE11, this project will no longer support IE either. Tested Browser: Chrome 107.0.5304.110


### Contribution

You can [Raise an issue](https://gitee.com/anolis/dsms-ui/issues/new) Or submit a Pull Request.

**Pull Request:**

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request