# dsms-ui

### 介绍
dsms-ui是为DSMS (Distributed Storage Management System) 分布式存储管理系统提供图形化界面的UI组件。

本组件与dsms-engine(管理引擎组件)配合，实现用户通过web页面使用、管理DSMS系统。

### 技术栈
- Vite
- Vue3
- Es6
- Element-plus
- Pinia
- Typescript
### 安装和使用
1.  获取代码
```
git clone https://gitee.com/anolis/dsms-ui.git
```
2.  安装依赖
```
cd dsms-ui
npm install
```
3. 运行
```
npm run dev
```
4.编译
```
npm run build
```

### 浏览器支持
本项目理论支持以下版本及更高版本的浏览器使用
| Edge      | Firefox      | Chrome      | Safari      |
| --------- | ------------ | ----------- | ----------- |
| Edge ≥ 79 | Firefox ≥ 78 | Chrome ≥ 64 | Safari ≥ 12 |
> 由于 Vue 3 不再支持 IE11，本项目也不再支持 IE 浏览器。测试浏览器：Chrome 107.0.5304.110


### 参与贡献

你可以[提一个 issue](https://gitee.com/anolis/dsms-ui/issues/new) 或者提交一个 Pull Request。

**Pull Request:**

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request