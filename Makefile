RPM_NAME=dsms-ui
RPM_VERSION=V1.0.0
RPM_RELEASE=V1.0
RPM_ARCH=x86_64
RPM_BUILD_DIR=$(shell pwd)/rpmbuild
RPM_SPEC_FILE=${RPM_BUILD_DIR}/SPECS/${RPM_NAME}.spec


.PHONY: clean build

clean:
	rm -rf target
	rm -rf ${RPM_BUILD_DIR}

build: clean
	npm install --force && npm run build
	mkdir -p ${RPM_BUILD_DIR}/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
	mkdir -p ${RPM_BUILD_DIR}/BUILD/dsms-ui/
	cp -r dsms-ui/* ${RPM_BUILD_DIR}/BUILD/dsms-ui/
	cp build/* ${RPM_BUILD_DIR}/BUILD/

	sed -e "s/@RPM_NAME@/${RPM_NAME}/g" \
	        -e "s/@RPM_VERSION@/${RPM_VERSION}/g" \
	        -e "s/@RPM_RELEASE@/${RPM_RELEASE}/g" \
	        -e "s/@RPM_ARCH@/${RPM_ARCH}/g" \
        	-e "s|@RPM_BUILD_DIR@|${RPM_BUILD_DIR}|g" \
	        build/dsms-ui.spec > ${RPM_SPEC_FILE}


	rpmbuild -bb ${RPM_SPEC_FILE}

	mkdir target

	find ${RPM_BUILD_DIR}/RPMS -type f -name "*.rpm" -exec cp {} ./target \;

	rm -rf ${RPM_BUILD_DIR}


