export interface clusterInfoState {
    clusterName: string;
    clusterAddress: string;
    clusterPort: string;
    authKey: string
}