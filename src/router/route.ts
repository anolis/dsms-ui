import { RouteRecordRaw } from 'vue-router';
import { eeRoutes } from './routeEE';
import { nameToTitle } from './routeTitle';
/**
 * meta: {
 *      title:          Menu bar, tagsView bar, menu search name (international)
 *      isLink：        Hyperlink menu or not, enable external chain conditions
 *      isHide：        Whether to hide this route
 *      isKeepAlive：   Whether to cache component status
 *      isAffix：       Fixed on the tagsView column
 *      isIframe：      Whether to embed the window and open the condition
 *      roles：         The current route permission ID is used for role management
 *      icon：          Menu, tagsView icon
 * }
 */

//  Define Dynamic Routes
export const dynamicRoutes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: '/',
		component: () => import('/@/layout/index.vue'),
		redirect: '/home',
		meta: {
			isKeepAlive: true,
		},
		children: [
			{
				path: '/no-cluster',
				name: 'noCluster',
				component: () => import('/@/views/no-cluster/index.vue'),
				meta: {
					title: nameToTitle.noCluster.title,
					isLink: '',
					isHide: true,
					isKeepAlive: false,
					isAffix: true,
					isIframe: false,
					roles: ['admin', 'common'],
					icon: `iconfont icon-${nameToTitle.noCluster.icon}`,
				}
			},
			{
				path: '/home',
				name: 'home',
				component: () => import('/@/views/home/index.vue'),
				meta: {
					title: nameToTitle.home.title,
					isLink: '',
					isHide: false,
					isKeepAlive: false,
					isAffix: false,
					isIframe: false,
					roles: ['admin', 'common'],
					icon: `iconfont icon-${nameToTitle.home.icon}`,
				},
			},
			{
				path: '/cluster',
				name: 'cluster',
				component: () => import('/@/layout/index.vue'),
				meta: {
					title: nameToTitle.cluster.title,
					isLink: '',
					isHide: false,
					isKeepAlive: false,
					isAffix: false,
					isIframe: false,
					roles: ['admin'],
					icon: `iconfont icon-${nameToTitle.cluster.icon}`,
				},
				children: [
					{
						path: '/cluster/node',
						name: 'node',
						component: () => import('/@/views/cluster/node/index.vue'),
						meta: {
							title: nameToTitle.node.title,
							isLink: '',
							isHide: false,
							isKeepAlive: true,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.node.icon}`,
						}
					},
					{
						path: '/cluster/storage',
						name: 'storage',
						component: () => import('/@/views/cluster/storage/index.vue'),
						meta: {
							title: nameToTitle.storage.title,
							isLink: '',
							isHide: false,
							isKeepAlive: true,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.storage.icon}`,
						}
					}
				]
			},
			{
				path: '/block-storage',
				name: 'blockStorage',
				component: () => import('/@/layout/index.vue'),
				meta: {
					title: nameToTitle.blockStorage.title,
					isLink: '',
					isHide: false,
					isKeepAlive: false,
					isAffix: false,
					isIframe: false,
					roles: ['admin'],
					icon: `iconfont icon-${nameToTitle.blockStorage.icon}`,
				},
				children: [
					{
						path: '/block-storage/storage-volume',
						name: 'storageVolume',
						component: () => import('/@/views/block-storage/storage-volume/index.vue'),
						meta: {
							title: nameToTitle.storageVolume.title,
							isLink: '',
							isHide: false,
							isKeepAlive: false,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.storageVolume.icon}`,
						},
					}
				]
			},
			{
				path: '/file-storage',
				name: 'fileStorage',
				component: () => import('/@/layout/index.vue'),
				meta: {
					title: nameToTitle.fileStorage.title,
					isLink: '',
					isHide: false,
					isKeepAlive: false,
					isAffix: false,
					isIframe: false,
					roles: ['admin'],
					icon: `iconfont icon-${nameToTitle.fileStorage.icon}`,
				},
				children: [
					{
						path: '/file-storage/file-system',
						name: 'fileSystem',
						component: () => import('/@/views/file-storage/file-system/index.vue'),
						meta: {
							title: nameToTitle.fileSystem.title,
							isLink: '',
							isHide: false,
							isKeepAlive: false,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.fileSystem.icon}`,
						},
					},
					{
						path: '/file-storage/directory',
						name: 'directory',
						component: () => import('/@/views/file-storage/directory/index.vue'),
						meta: {
							title: nameToTitle.directory.title,
							isLink: '',
							isHide: false,
							isKeepAlive: false,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.directory.icon}`,
						},
					}
				]
			},
			{
				path: '/alarm',
				name: 'alarm',
				component: () => import('/@/layout/index.vue'),
				meta: {
					title: nameToTitle.alarm.title,
					isLink: '',
					isHide: false,
					isKeepAlive: false,
					isAffix: false,
					isIframe: false,
					roles: ['admin'],
					icon: `iconfont icon-${nameToTitle.alarm.icon}`,
				},
				children: [
					{
						path: '/alarm/info',
						name: 'alarmInfo',
						component: () => import('/@/views/alarm/info/index.vue'),
						meta: {
							title: nameToTitle.alarmInfo.title,
							isLink: '',
							isHide: false,
							isKeepAlive: true,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.alarmInfo.icon}`,
						}
					},
					{
						path: '/alarm/setting',
						name: 'alarmSetting',
						component: () => import('/@/views/alarm/setting/index.vue'),
						meta: {
							title: nameToTitle.alarmSetting.title,
							isLink: '',
							isHide: false,
							isKeepAlive: true,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.alarmSetting.icon}`,
						}
					},
				]
			},
			{
				path: '/monitor-center',
				name: 'monitorCenter',
				component: () => import('/@/layout/index.vue'),
				meta: {
					title: nameToTitle.monitorCenter.title,
					isLink: '',
					isHide: false,
					isKeepAlive: false,
					isAffix: false,
					isIframe: false,
					roles: ['admin'],
					icon: `iconfont icon-${nameToTitle.monitorCenter.icon}`,
				},
				children: [
					{
						path: '/monitor-center/cluster-status',
						name: 'clusterStatus',
						component: () => import('/@/views/monitor-center/cluster-status/index.vue'),
						meta: {
							title: nameToTitle.clusterStatus.title,
							isLink: '',
							isHide: false,
							isKeepAlive: true,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.clusterStatus.icon}`,
						}
					},
					{
						path: '/monitor-center/cluster-performance',
						name: 'clusterPerformance',
						component: () => import('/@/views/monitor-center/cluster-performance/index.vue'),
						meta: {
							title: nameToTitle.clusterPerformance.title,
							isLink: '',
							isHide: false,
							isKeepAlive: true,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.clusterPerformance.icon}`,
						}
					},
				]
			},
			{
				path: '/system-event',
				name: 'systemEvent',
				component: () => import('/@/layout/index.vue'),
				meta: {
					title: nameToTitle.systemEvent.title,
					isLink: '',
					isHide: false,
					isKeepAlive: false,
					isAffix: false,
					isIframe: false,
					roles: ['admin'],
					icon: `iconfont icon-${nameToTitle.systemEvent.icon}`,
				},
				children: [
					{
						path: '/system-event/task',
						name: 'task',
						component: () => import('/@/views/system-event/task/index.vue'),
						meta: {
							title: nameToTitle.task.title,
							isLink: '',
							isHide: false,
							isKeepAlive: true,
							isAffix: false,
							isIframe: false,
							roles: ['admin'],
							icon: `iconfont icon-${nameToTitle.task.icon}`,
						}
					},
				]
			},
		],
	},
];
// Load closed source function route
for (let eRoute of eeRoutes) {
	const pChildren = dynamicRoutes[0].children as RouteRecordRaw[];
	const index = pChildren.findIndex((route:any) => route.name === eRoute.name);
	if (index === -1) {
		dynamicRoutes[0].children = [...pChildren, eRoute];
	} else {
		((dynamicRoutes[0].children as RouteRecordRaw[])[index].children = [...(pChildren[index].children as RouteRecordRaw[]), ...(eRoute.children as RouteRecordRaw[])]);
	}
}

export const notFoundAndNoPower = [
	{
		path: '/:path(.*)*',
		name: 'notFound',
		component: () => import('/@/views/error/404.vue'),
		meta: {
			title: '找不到此页面',
			isHide: true,
		},
	},
	{
		path: '/401',
		name: 'noPower',
		component: () => import('/@/views/error/401.vue'),
		meta: {
			title: '没有权限',
			isHide: true,
		},
	},
];

// Define static routes
export const staticRoutes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: '/',
		component: () => import('/@/layout/index.vue'),
		meta: {
			title: '布局界面',
		},
		children: [
			...notFoundAndNoPower,
		],
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('/@/views/login/index.vue'),
		meta: {
			title: '登录',
		},
	},
];
