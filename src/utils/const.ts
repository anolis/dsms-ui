import { mapData } from '../types';

export const opsMap:mapData = {
	read: '读',
	write: '写'
};

export const osdDelayMap:mapData = {
	commit: '提交',
	apply: '应用'
};

export const loadMap:mapData = {
	write: '元数据请求负载'
};

export const pgMap:mapData = {
	activePg: 'active',
	degradedPg: 'degraded',
	downPg: 'down',
	inactivePg: 'inactive',
	inconsistentPg: 'inconsistent',
	undersizedPg: 'undersized',
	totalPg: 'total',
	unknownPg: 'unknown'
};

export const memoryMap:mapData = {
	totalMemory: '总内存',
	usedMemory: '已使用',
};

export const timeNameMap:mapData = {
	'a minute': '1 分钟',
	'5 minutes': '5 分钟',
	'10 minutes': '10 分钟',
	'15 minutes': '15 分钟',
	'30 minutes': '30 分钟',
	'an hour': '1 小时',
	'3 hours': '3 小时',
	'6 hours': '6 小时',
	'12 hours': '12 小时',
	'a day': '1 天',
	'3 days': '3 天',
	'7 days': '7 天',
	'a month': '1 个月',
	'3 months': '3 个月',
	'6 months': '6 个月',
	'a year': '1 年',
	'3 years': '3 年',
};

export const alarmMap:{
	[key:string]:string
} = {
	0: 'SLOW_OPS',
	1: 'HEALTH_WARN',
	2: 'HEALTH_ERROR',
};