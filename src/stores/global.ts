import { defineStore } from 'pinia';
import { useAlarmApi } from '/@/api/alarm';

export const useGlobal = defineStore('global', {
	state: () => ({
		uncomfirmMessage: 0
	}),
	actions: {
		setUncomfirmMessage(data: number) {
			this.uncomfirmMessage = data;
		},
		fetchUncomfirmMessage() {
			useAlarmApi().getUnconfirmMessageNum().then((res) => {
				this.uncomfirmMessage = res.data;
			});
		}
	}
});