import request from '/@/utils/request';

export function useMonitorApi() {
	return {
		// cluster status list
		monitorList: () => {
			return request({
				url: '/api/monitor/list_mon_status',
				method: 'post',
			});
		},
		// cluster performance
		getClusterMon: (data:object) => {
			return request({
				url: '/api/monitor/get_cluster_mon',
				method: 'post',
				data,
				timeout: 30000
			});
		},
		// node performance
		getNodeMon: (data:object) => {
			return request({
				url: '/api/monitor/get_node_mon',
				method: 'post',
				data
			});
		},
		// pool performance
		getPoolMon: (data:object) => {
			return request({
				url: '/api/monitor/get_pool_mon',
				method: 'post',
				data
			});
		},
		// open rbd performance
		openRbdMon: (data:object) => {
			return request({
				url: '/api/monitor/enable_rbd_mon',
				method: 'post',
				data
			});
		},
		// rbd performance
		getRbdMon: (data:object) => {
			return request({
				url: '/api/monitor/get_rbd_mon',
				method: 'post',
				data
			});
		},
		// cephfs performance
		getCephfsMon: (data:object) => {
			return request({
				url: '/api/monitor/get_cephfs_mon',
				method: 'post',
				data,
				timeout: 30000
			});
		},
		// osd performance
		getOsdMon: (data:object) => {
			return request({
				url: '/api/monitor/get_osd_mon',
				method: 'post',
				data
			});
		},
	};
}
