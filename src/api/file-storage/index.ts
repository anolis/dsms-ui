import request from '/@/utils/request';

export function useFileStorageApi() {
	return {
		getFileSystemList: () => {
			return request({
				url: '/api/cephfs/list',
				method: 'post',
			});
		},
		addFileSystem: (params: object) => {
			return request({
				url: '/api/cephfs/create_fs',
				method: 'post',
				data: params
			});
		},
		delFileSystem: (params: object) => {
			return request({
				url: '/api/cephfs/delete_fs',
				method: 'post',
				data: params
			});
		},
		getStorageDirList: (data:{
			pageNo: number,
			pageSize: number,
			storageDirName?:string,
		}) => {
			return request({
				url: '/api/storagedir/list',
				method: 'post',
				data
			});
		},
		addStorageDir: (params: object) => {
			return request({
				url: '/api/storagedir/create_dir',
				method: 'post',
				data: params
			});
		},
		delStorageDir: (params: object) => {
			return request({
				url: '/api/storagedir/delete_dir',
				method: 'post',
				data: params
			});
		},
		getStorageDirGuide: () => {
			return request({
				url: '/api/storagedir/guide',
				method: 'post'
			});
		},
	};
}