import request from '/@/utils/request';

export function useClusterApi() {
	return {
		// query cluster
		getCluster: (params?: object) => {
			return request({
				url: '/api/cluster/about',
				method: 'post',
				data: params,
				headers: {
					tip: 'no'
				}
			});
		},
		// binding cluster
		bindingCluster: (params: object) => {
			return request({
				url: '/api/cluster/bind',
				method: 'post',
				data: params,
			});
		},
		// unbinding cluster
		unbindcluster: (params?: object) => {
			return request({
				url: '/api/cluster/unbind',
				method: 'post',
				data: params
			});
		}
	};
}