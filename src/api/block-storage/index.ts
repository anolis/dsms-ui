import request from '/@/utils/request';

export function useBlockStorageApi() {
	return {
		getVolumeList: (params:{
			poolName?:string,
			rbdName?:string,
			pageNo?:number,
			pageSize?:number
		}) => {
			return request({
				url: '/api/volume/list',
				method: 'post',
				data: params,
				timeout: 30000
			});
		},
		addVolume: (params:{
      poolName:string|null,
      rbdName:string,
      rbdSize:string,
			basedOnEra:boolean,
    }) => {
			return request({
				url: '/api/volume/create_rbd',
				method: 'post',
				data: params
			});
		},
		delVolume: (params:{
      poolName:string | undefined,
      rbdName:string | undefined,
    }) => {
			return request({
				url: '/api/volume/delete_rbd',
				method: 'post',
				data: params
			});
		},
		updataVolume: (params:{
      poolName:string,
      rbdName:string,
			rbdSize:string
    }) => {
			return request({
				url: '/api/volume/update_rbd',
				method: 'post',
				data: params
			});
		},
		downloadKey: () => {
			return request({
				url: '/api/volume/download_key',
				method: 'post',
			});
		},
		getPool: (params:{
      poolName:string | undefined,
    }) => {
			return request({
				url: '/api/pool/get',
				method: 'post',
				data: params
			});
		}
	};
}