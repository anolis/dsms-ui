import request from '/@/utils/request';

export function useNodeApi() {
	return {
		// node list
		nodeList: (data:{
			pageNo?: number,
			pageSize?: number,
			nodeName?: string,
			nodeStatus?: number,
		}) => {
			return request({
				url: '/api/node/list',
				method: 'post',
				data,
				timeout: 30000
			});
		},
		// node info
		nodeInfo: (params: object) => {
			return request({
				url: '/api/node/get',
				method: 'post',
				data: params,
			});
		},
		// add disk to node
		addDiskToNode: (params: object) => {
			return request({
				url: '/api/node/add_osd',
				method: 'post',
				data: params,
			});
		},
		// remove disk from node
		removeDiskFromNode: (params: object) => {
			return request({
				url: '/api/node/remove_osd',
				method: 'post',
				data: params,
			});
		},
	};
}
