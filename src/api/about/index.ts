import request from '/@/utils/request';

export function useAboutApi() {
	return {
		// get version
		getComponentsVersion: () => {
			return request({
				url: '/api/component/about',
				method: 'post'
			});
		},
	};
}
