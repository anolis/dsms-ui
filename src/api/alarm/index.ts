import request from '/@/utils/request';

export function useAlarmApi() {
	return {
		getAlarmStrategyList: (data:object) => {
			return request({
				url: '/api/alertrule/list',
				method: 'post',
				data
			});
		},

		getAlarmStrategy: (ruleId: number | null) => {
			return request({
				url: '/api/alertrule/get',
				method: 'post',
				data: { ruleId }
			});
		},

		updateAlarmStrategy: (data: Object) => {
			return request({
				url: '/api/alertrule/update_rule',
				method: 'post',
				data
			});
		},

		resetAlarmStrategy: (ruleId: number) => {
			return request({
				url: '/api/alertrule/reset_rule',
				method: 'post',
				data: { ruleId }
			});
		},

		getAlarmAppriseList: (data: object) => {
			return request({
				url: '/api/alertapprise/list',
				method: 'post',
				data
			});
		},

		updateAlarmApprise: (data: object) => {
			return request({
				url: '/api/alertapprise/update_apprise',
				method: 'post',
				data
			});
		},

		addAlarmContact: (data: {
			contactAccount:string,
			contactName:string,
			contactType:number
		}) => {
			return request({
				url: '/api/alertcontact/create',
				method: 'post',
				data
			});
		},

		deleteAlarmContact: (data: {contactId:number}) => {
			return request({
				url: '/api/alertcontact/delete',
				method: 'post',
				data
			});
		},

		sendEmailTest: () => {
			return request({
				url: '/api/alertapprise/send_email',
				method: 'post',
			});
		},

		sendSmsTest: () => {
			return request({
				url: '/api/alertapprise/send_sms',
				method: 'post',
			});
		},

		getAlertMessageList: (data: {
			level?: string | number | undefined | null,
			model?: string | undefined | null,
			pageNo: string | number,
			pageSize: string | number,
			status?: string | number | undefined | null,
		}) => {
			return request({
				url: '/api/alertmessage/list',
				method: 'post',
				data
			});
		},

		confirmAlertMessage: (data: {
			ids: number[]
		}) => {
			return request({
				url: '/api/alertmessage/confirm_message',
				method: 'post',
				data
			});
		},

		delAlertMessage: (data: {
			ids: number[]
		}) => {
			return request({
				url: '/api/alertmessage/delete_message',
				method: 'post',
				data
			});
		},

		getUnconfirmMessageNum: () => {
			return request({
				url: '/api/alertmessage/get_unconfirmed_message_num',
				method: 'post',
			});
		},
	};
}