import request from '/@/utils/request';

export function useTaskApi() {
	return {
		// task list
		taskList: (params: object) => {
			return request({
				url: '/api/task/list',
				method: 'post',
				data: params
			});
		},
	};
}
