import request from '/@/utils/request';

export function useHomeApi() {
	return {
		getOverview: () => {
			return request({
				url: '/api/overview/get',
				method: 'post'
			});
		},
	};
}
