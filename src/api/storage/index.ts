import request from '/@/utils/request';

export function useStorageApi() {
	return {
		// get pool list
		getPoolList: (data: {
			pageNo?: number,
			pageSize?: number,
			fileSystem?: string,
			poolName?: string,
			poolType?: number | string,
			rbd?: string[] | null,
			isAvailable?:boolean
		}) => {
			return request({
				url: '/api/pool/list',
				method: 'post',
				data,
				timeout: 30000
			});
		},
		// get pool_disk list
		getPoolDiskList: (data: object) => {
			return request({
				url: '/api/pool/list_used_disk',
				method: 'post',
				data
			});
		},
		// create storage_pool
		createStoragePool: (data: object) => {
			return request({
				url: '/api/pool/create_pool',
				method: 'post',
				data
			});
		},
		// add storage_pool disk
		addStoragePoolDisk: (data: object) => {
			return request({
				url: '/api/pool/add_disk',
				method: 'post',
				data
			});
		},
		// remove storage_pool disk
		removeStoragePoolDisk: (data: object) => {
			return request({
				url: '/api/pool/remove_disk',
				method: 'post',
				data
			});
		},
		// delete storage pool
		deleteStoragePool: (data: object) => {
			return request({
				url: '/api/pool/delete_pool',
				method: 'post',
				data
			});
		},
		// add node
		addNode: (data:object) => {
			return request({
				url: '/api/pool/add_node',
				method: 'post',
				data
			});
		},
		removeNode: (data:object) => {
			return request({
				url: 'api/pool/remove_node',
				method: 'post',
				data
			});
		},
		getUnusedDisk: (data:object) => {
			return request({
				url: '/api/pool/list_unused_disk',
				method: 'post',
				data
			});
		}
	};
}