export interface StepTableItem{
	step: string,
	operate: string,
	example: string
}
const certificate = <StepTableItem[]>([
	{
		step: '下载证书',
		operate: '下载证书到本机',
		example: 'download'
	},
	{
		step: '拷贝证书',
		operate: '拷贝证书到/etc/ceph目录下',
		example: '# scp /location/to/ceph.client.admin.keyring {user}@{host}:/etc/ceph'
	}
]);
const volumeMount = <StepTableItem[]>([
	{
		step: '存储卷映射',
		operate: '在客户机映射存储卷',
		example: '# rbd map pool1/rbd0'
	},
	{
		step: '关闭特性',
		operate: '有部分系统执行存储卷映射时会映射失败，需要关闭某些特性才能正常映射，执行关闭特性后需再次执行存储卷映射命令',
		example: '# rbd feature disable pool1/rbd0 object-map fast-diff deep-flatten'
	},
	{
		step: '格式化存储卷',
		operate: '在客户机格式化存储卷（仅首次挂载时需要）',
		example: '# mkfs.ext4 /dev/rbd0'
	},
	{
		step: '创建挂载点',
		operate: '在客户机创建挂载点',
		example: '# mkdir -p /mnt/rbd0'
	},
	{
		step: '存储卷挂载',
		operate: '在客户机挂载存储卷',
		example: '# mount /dev/rbd0 /mnt/rbd0'
	}
]);
const volumeUninstall = <StepTableItem[]>([
	{
		step: '存储卷卸载',
		operate: '在客户机卸载存储卷',
		example: '# umount /mnt/rbd0'
	},
	{
		step: '存储卷取消映射',
		operate: '在客户机取消映射存储卷',
		example: '# rbd unmap pool1/rbd0'
	},
	{
		step: '删除挂载点',
		operate: '在客户机删除挂载点',
		example: '# rm -rf /mnt/rbd0'
	}
]);
const fileSystemResize = <StepTableItem[]>([
	{
		step: '卸载存储卷',
		operate: '在客户机卸载存储卷',
		example: '# umount /mnt/rbd0'
	},
	{
		step: '检查文件系统是否正常',
		operate: '在客户机检查文件系统是否正常',
		example: '# e2fsck -f /dev/rbd0'
	},
	{
		step: '更新文件系统容量(缩容执行)',
		operate: '在客户机更新文件系统容量',
		example: '# resize2fs /dev/rbd0 {size}'
	},
	{
		step: '存储卷扩/缩容',
		operate: '在页面进行存储卷扩/缩容',
		example: '页面操作'
	},
	{
		step: '更新文件系统容量(扩容执行)',
		operate: '在客户机通知系统同步更新',
		example: '# resize2fs /dev/rbd0'
	}
]);
const guideData = { certificate, volumeMount, volumeUninstall, fileSystemResize };

export default guideData;