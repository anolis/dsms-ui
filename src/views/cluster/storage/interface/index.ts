import { NodeDiskData } from '/@/types/index';
export interface storagePoolState {
    poolName: string;
    poolType: string;
    pgNum: number;
    pgpNum: number;
    disk: [];
    used: string;
    total: string
}

export interface storageTableState {
    storageTable:{
        loading: boolean;
        data: storagePoolState[];
        total: number;
        params: {
            pageNum: number;
            pageSize: number;
            poolName?: string;
        };
    };
}

export interface DiskItem {
    nodeName:string,
    usedDevice:number,
    totalDevice:string,
    nodeDevices: NodeDiskData[]
}