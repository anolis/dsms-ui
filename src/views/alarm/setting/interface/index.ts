export interface AlarmStrategyInterface {
    ruleId: number
    ruleName: string
    ruleMetric: number
    ruleThreshold: string
    ruleTimes: number
    ruleModule: string
    ruleLevel: number
    ruleStatus: number
}

export interface AlarmStrategStateInterface {
    alarmInfo:{
        data: AlarmStrategyInterface[]
        params: {
            pageSize: number
            pageNum: number
            ruleLevel: number | null
        }
        total: number
    }
    editAlarmData: EditAlarmDataInterface
}

export interface EmailAlarmSettingInterface {
    smtpHost: string
    smtpPort: number
    smtpUsername: string
    smtpPassword: string
    emailFrom: string
}

export interface SmsAlarmSettingInterface {
    smsProvider: string
    smsProviderHost: string
    smsRegion: string
    smsSignName: string
    smsAccessKeyId: string
    smsAccessKeySecret: string
    smsTemplateCode: string
}

export interface EditAlarmDataInterface {
	ruleThreshold: string | number | null
	ruleId: number | null
	ruleTimes: number | null
	ruleLevel: number | null
}